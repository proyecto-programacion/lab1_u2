<!DOCTYPE html>
<html class="no-js" lang="es">
	<head>
		<meta charset="UTF-8">
		<!-- nombre de la pagina-->
		<title>Ejercicio 3 Método GET</title>
		<link rel="stylesheet" href="../../estilo.css">
	</head>
	<body>
		<CENTER>
			<!-- Titulo de la pagina-->
			<h1 id="Título">TABLA: Método GET</h1>
			
			<FORM METHOD="GET" ACTION="procesar_get.php">
				<!-- Tse crea la tabla-->
				<TABLE>
					<!-- para ingresar el tamaño de la tabla-->
					<TR>
						<TD ALIGN="LEFT">Tamaño:</TD>
						<TD ALIGN="LEFT" COLSPAN="3"><INPUT TYPE="NUMBER" NAME="Tamaño" SIZE=5"></TD>
					</TR>
					<!-- para ingresar el color 1 de la tabla-->
					<TR>
						<TD ALIGN="LEFT">Color 1:</TD>
						<TD ALIGN="RIGHT" COLSPAN="3"><INPUT TYPE="COLOR" NAME="color1" SIZE=25 ></TD>
					</TR>
					<!-- para ingresar el color 2 de la tabla-->
					<TR>
						<TD ALIGN="LEFT">Color 2:</TD>
						<TD ALIGN="RIGHT" COLSPAN="3"><INPUT TYPE="COLOR" NAME="color2" SIZE=25></TD>
					</TR>
					
				</TABLE><HR><BR>
				
				<!-- botones para enviar los datos y para limpiar la pagina-->
				<INPUT TYPE="SUBMIT" value ="Enviar"> <INPUT TYPE="RESET">
				
			</FORM>
			
		</CENTER>
		
	</body>
 </html>

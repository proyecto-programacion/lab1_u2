<!DOCTYPE html>
<html class="no-js" lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Ejercicio 3 Método GET</title>
		<link rel="stylesheet" href="../../estilo.css">
	</head>
	<body>
		<h1 id="Título">TABLA NXN</h1>
		<?php
		$metodo = $_SERVER["REQUEST_METHOD"];
		$cad_consulta = $_SERVER['QUERY_STRING'];
  
		echo "<H2>Tabla: método $metodo</H2>";
		echo "<I>Query String</I>: $cad_consulta <HR>";
		
		#Se obtienen los valores de las variables  
		$Tamaño = $_GET["Tamaño"];
		#Si ponen un numero negativo se transformará en positivo
		if($Tamaño<0){
			$Tamaño = $Tamaño *-1;
		}
		# si es 0 no se crea la tabla
		if($Tamaño==0){
			echo "El tamaño de la tabla es 0, no se puede realizar la tabla";
		}
		$color1 = $_GET["color1"];
		$color2 = $_GET["color2"];
		
		# se crea la tabala
		echo "<table border=1>";
		$n=1;
		for ($n1=1; $n1<=$Tamaño; $n1++){
			# Si la división de la variable $n1 entre dos da 0 creamos una fila del color1
			if ($n1 % 2 == 0){
				echo "<tr bgcolor=$color1>";
			}
			#creamos fila del color2 cuando no sea divisible entre 2
			else{
				echo "<tr bgcolor=$color2>";
			}
			for ($n2=1; $n2<=$Tamaño; $n2++){
				# creamos una celda y mostramos el número
				echo "<td>", $n, "</td>";
				#sumamos 1 al número mostrado para la siguiente vuelta del ciclo
				$n=$n+1;
			}
			# cerramos la fila
			echo "</tr>";
		}
		#cerramos la tabla
		echo "</table>"; 
		?>
 	</body>
 </html>

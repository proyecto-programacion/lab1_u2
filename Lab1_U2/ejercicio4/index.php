<!DOCTYPE html>

<html class="no-js" lang="es">
	<head>
		<meta charset="UTF-8">
		<title>Ejercicio 4</title>
		<link rel="stylesheet" href="../estilo.css">
	</head>
	<body>
		<h1 id="Título">FOTOS</h1>
		<table class="tabla">
			<tr>
			<?php
			
			#se crea el contador
			$cont = 0;
			# se crea la ruta para llegar a las imagenes
			$ruta = "fotos/";
			
			# se abre la carpeta con las imagenes
			$carpeta = opendir($ruta);
			
			# ciclo para recorrer la carpeta e insertar las imagenes
			while ($foto = readdir($carpeta)) {
				if ($foto != "." && $foto != "..") {
					?>
					<Td><img src=<?php echo $ruta.$foto ?> width="200px"></Td>
					<?php
					$cont = $cont + 1;
        // si el contador llega a multiplos de 4 se termina la fila y pasa a otra columna
					if($cont%4 == 0){
						echo('</tr>');
					}
				} 
			}
    // se cierra la carpeta de archivos
			closedir($carpeta);
			?>
		</table>
	</body>
 </html>
